package com.ad2pro.spectra.jobscheduler.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "com.ad2pro.spectra.job")
@Data
public class JobProperties {
    private final String baseTarget;
    private final String source;
}
