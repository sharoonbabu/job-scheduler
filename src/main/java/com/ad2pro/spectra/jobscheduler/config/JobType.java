package com.ad2pro.spectra.jobscheduler.config;

public enum JobType {
    NORMAL,
    RUSH
}
