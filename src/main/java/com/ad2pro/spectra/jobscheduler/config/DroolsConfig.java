package com.ad2pro.spectra.jobscheduler.config;

import lombok.extern.slf4j.Slf4j;
import org.kie.api.KieServices;
import org.kie.api.builder.KieScanner;
import org.kie.api.builder.ReleaseId;
import org.kie.api.runtime.KieContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
@ComponentScan("com.ad2pro.spectra.jobscheduler")
public class DroolsConfig {

    @Bean
    public KieContainer kieContainer() {
        KieServices kieServices = KieServices.Factory.get();
        ReleaseId releaseId = kieServices.newReleaseId("com.ad2pro.spectra", "drools", "1.0.0");
        KieContainer kieContainer = kieServices.newKieContainer(releaseId);
        KieScanner kieScanner = kieServices.newKieScanner(kieContainer);
        kieScanner.start(5000L);
        return kieContainer;
    }
}
