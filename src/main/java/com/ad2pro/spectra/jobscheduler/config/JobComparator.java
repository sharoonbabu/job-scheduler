package com.ad2pro.spectra.jobscheduler.config;

import com.ad2pro.spectra.jobscheduler.domain.Job;

import java.util.Comparator;

public class JobComparator implements Comparator<Job> {
    @Override
    public int compare(Job o1, Job o2) {
        if(o1.getJobType().equals(o2.getJobType()))
            return 1;
        else if(o1.getJobType().equals(JobType.RUSH)) return -1;
        else return 1;
    }
}
