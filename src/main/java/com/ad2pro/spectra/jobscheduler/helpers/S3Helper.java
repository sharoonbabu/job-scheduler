package com.ad2pro.spectra.jobscheduler.helpers;

import com.ad2pro.spectra.aws.config.AWSConfig;
import com.ad2pro.spectra.aws.utils.S3ServiceUtil;
import com.ad2pro.spectra.jobscheduler.domain.Job;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
@Slf4j
public class S3Helper {

    @Autowired
    private S3ServiceUtil s3ServiceUtil;

    @Autowired
    private AWSConfig awsConfig;

    public boolean uploadJob(Job job) {
        String key = getS3Key(job);
        job.setS3Key(key);
        return s3ServiceUtil.upload(awsConfig.getBucket(),key,job.getPath());
    }

    private String getS3Key(Job job) {
        String s3Key = job.getSiteId() + File.separator;
        log.info("S3 key {}", s3Key);
        return s3Key;
    }

}
