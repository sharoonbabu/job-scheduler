package com.ad2pro.spectra.jobscheduler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.ad2pro.spectra")
@Slf4j
public class JobScheduler {
    public static void main(String[] args) {
        SpringApplication.run(JobScheduler.class, args);
    }
}
