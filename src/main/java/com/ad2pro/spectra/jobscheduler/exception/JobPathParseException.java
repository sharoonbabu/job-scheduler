package com.ad2pro.spectra.jobscheduler.exception;

/**
 * @author Sharoon Babu
 * Package: com.ad2pro.spectra.jobscheduler.exception
 * Date: 20/6/19
 */
public class JobPathParseException extends Exception {

    public JobPathParseException(String message) {
        super(message);
    }
}
