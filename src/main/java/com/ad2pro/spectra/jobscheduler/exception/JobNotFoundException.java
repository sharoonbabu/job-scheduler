package com.ad2pro.spectra.jobscheduler.exception;

public class JobNotFoundException  extends Exception{

    public JobNotFoundException(String message) {
        super(message);
    }
}
