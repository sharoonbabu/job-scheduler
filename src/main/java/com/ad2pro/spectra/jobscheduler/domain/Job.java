package com.ad2pro.spectra.jobscheduler.domain;

import com.ad2pro.spectra.jobscheduler.config.JobType;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Data
@Slf4j
public class Job  implements Serializable {

    private static final long serialVersionUID = 972642599983719185L;

    private String trackingID;
    private Long siteId;
    private String name;
    private String path;
    private String s3Key;
    private Long fileSize;
    private JobType jobType;
    private boolean possibleDup;

    public Job(String trackingID, String name, String path, Long fileSize, boolean possibleDup) {
        this.trackingID = trackingID;
        this.name = name;
        this.path = path;
        this.fileSize = fileSize;
        this.possibleDup = possibleDup;
    }
}
