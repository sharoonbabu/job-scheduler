package com.ad2pro.spectra.jobscheduler;

import com.ad2pro.spectra.jobscheduler.config.JobComparator;
import com.ad2pro.spectra.jobscheduler.domain.Job;
import com.ad2pro.spectra.jobscheduler.runnable.JobConsumer;
import com.ad2pro.spectra.jobscheduler.runnable.JobProducer;
import com.ad2pro.spectra.jobscheduler.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

@Component
@Slf4j
public class JobSchedulerStartup implements ApplicationListener<ApplicationReadyEvent > {

    @Autowired
    @Qualifier("consumerPoolExecutor")
    private TaskExecutor taskExecutor;

    private BlockingQueue<Job> jobQueue  = new PriorityBlockingQueue<>(Constants.QUEUE_MAX_SIZE, new JobComparator());

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        jobScheduler();
    }

    private void jobScheduler() {
        produceJobs();

        consumeJobs();
    }

    private void produceJobs() {
        JobProducer jobProducer = new JobProducer(jobQueue);
        Executors.newSingleThreadExecutor().submit(jobProducer);
    }

    private void consumeJobs() {
        JobConsumer jobConsumer = new JobConsumer(jobQueue);
        for (int count = 0; count < Constants.MAX_THREADS; count++) {
            taskExecutor.execute(jobConsumer);
        }
    }

}
