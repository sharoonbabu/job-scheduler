package com.ad2pro.spectra.jobscheduler.runnable;

import com.ad2pro.spectra.jobscheduler.config.JobProperties;
import com.ad2pro.spectra.jobscheduler.domain.Job;
import com.ad2pro.spectra.jobscheduler.exception.JobPathParseException;
import com.ad2pro.spectra.jobscheduler.processors.QueueProcessor;
import com.ad2pro.spectra.jobscheduler.service.JobProcessingService;
import com.ad2pro.spectra.jobscheduler.utils.ApplicationContextUtils;
import com.ad2pro.spectra.jobscheduler.utils.FileUtils;
import com.ad2pro.spectra.jobscheduler.utils.ParseUtil;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import static com.ad2pro.spectra.jobscheduler.utils.FileUtils.listFiles;

@Slf4j
public class JobProducer implements Runnable {

    private BlockingQueue<Job> jobQueue;
    private JobProperties jobProperties;
    private QueueProcessor queueProcessor;
    private JobProcessingService jobProcessingService;

    public JobProducer(BlockingQueue<Job> jobQueue) {
        this.jobQueue = jobQueue;
        this.jobProperties = ApplicationContextUtils.getApplicationContext(JobProperties.class);
        this.jobProcessingService = ApplicationContextUtils.getApplicationContext(JobProcessingService.class);
        this.queueProcessor = ApplicationContextUtils.getApplicationContext(QueueProcessor.class);
    }

    @PostConstruct
    private void produceFailedEvents() {
        String failedJobsDir = jobProperties.getBaseTarget();
        List<File> failedEventFiles = new ArrayList<>();
        listFiles(failedJobsDir,failedEventFiles);
        for (File job : failedEventFiles) {
            try {
                String jobPath = job.getPath();
                log.info("Reproducing event for failed event file {}", jobPath);
                long siteID = ParseUtil.getSiteId(job.getPath());
                processJobOrder(job, siteID);
                log.info("Produced Job into Queue {} by {}", job.getName(), Thread.currentThread().getName());
            } catch (IOException e) {
                log.error("Failed to get attributes of file {}", e.getMessage());
            } catch (InterruptedException e) {
                log.error("Exception occurred {}", e.getMessage());
            } catch (JobPathParseException e) {
                log.error("Exception occurred while parsing siteID {}", e.getMessage());
            }
        }
    }

    @Override
    public void run() {
        while (true) {
            List<String> jobDirs = listJobs();
            for (String jobDir : jobDirs) {
                try {
                    long siteID = ParseUtil.getSiteId(jobDir);
                    log.info("Working on jobs for siteID {}", siteID);
                    List<File> jobPaths = new ArrayList<>();
                    listFiles(jobDir, jobPaths);
                    for (File job : jobPaths) {
                        log.info("Current job {}", job);
                        try {
                            processJobOrder(job, siteID);
                            log.info("Produced Job into Queue {} by {}", job.getName(), Thread.currentThread().getName());
                        } catch (IOException e) {
                            log.error("Exception occurred for job {}, {}",job, e.getMessage());
                        }
                    }
                } catch (InterruptedException e) {
                    log.error("Exception occurred {}", e.getMessage());
                } catch (JobPathParseException e) {
                    log.error("Error occurred while parsing job path {}", e.getMessage() );
                }
            }
        }
    }

    private void processJobOrder(File job, long siteID) throws IOException, InterruptedException {
        String currentJobPath = job.getPath();
        Job jobInfo = jobProcessingService.buildJobMetadata(siteID, currentJobPath, false);
        FileUtils.move(currentJobPath, jobInfo.getPath());
        queueProcessor.pushIntoQueue(jobQueue, jobInfo);
    }

    private List<String> listJobs() {
     // TODO: Get job locations for all sites present on FTP
        List<String> dirs = new ArrayList<>();
        dirs.add("/home/sharoonbabu/jobs/338");
        dirs.add("/home/sharoonbabu/jobs/78");
        return dirs;
    }
}
