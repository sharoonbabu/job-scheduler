package com.ad2pro.spectra.jobscheduler.runnable;

import com.ad2pro.spectra.jobscheduler.domain.Job;
import com.ad2pro.spectra.jobscheduler.service.JobProcessingService;
import com.ad2pro.spectra.jobscheduler.utils.ApplicationContextUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.BlockingQueue;

@Slf4j
public class JobConsumer implements Runnable {

    private BlockingQueue<Job> jobQueue;
    private JobProcessingService jobProcessingService;

    public JobConsumer(BlockingQueue<Job> jobQueue) {
        this.jobQueue = jobQueue;
        jobProcessingService = ApplicationContextUtils.getApplicationContext(JobProcessingService.class);
    }

    @Override
    public void run() {
        while(true) {
            try {
                Job job = jobQueue.take();
                log.info("Job {} assigned to {}", job.getName(), Thread.currentThread().getName());
                log.info("Implement your job processing here");
                jobProcessingService.processJobOrder(job);
            } catch(InterruptedException | IllegalMonitorStateException e) {
                log.error("Exception occurred {}", e.getMessage());
            } catch (Exception e) {
                log.error("Exception occurred {}", e.getMessage());
            }
        }
    }
}
