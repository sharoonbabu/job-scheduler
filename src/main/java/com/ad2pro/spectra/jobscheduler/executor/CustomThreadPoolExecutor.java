package com.ad2pro.spectra.jobscheduler.executor;

import com.ad2pro.spectra.jobscheduler.domain.Job;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Data
@Slf4j
@Configuration
@EnableAsync
public class CustomThreadPoolExecutor {

    @Bean(name = "consumerPoolExecutor")
    public TaskExecutor initializeThreadPool() {
        ThreadPoolTaskExecutor taskExecutor =  new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(5);
        taskExecutor.setMaxPoolSize(20);
        taskExecutor.setThreadNamePrefix("consumer-thread-");
        taskExecutor.setRejectedExecutionHandler((r, executor) -> {
            log.info("Job Rejected : {} , Wait the process.", ((Job) r).getName());
            waitThread();
            log.info("Add another time : {} ", ((Job) r).getName());
            executor.execute(r);
        });
        taskExecutor.initialize();
        return taskExecutor;
    }

    private void waitThread() {
        try {
            log.info("Waiting for a second !!");
            Thread.sleep(1);
        } catch (InterruptedException e) {
            log.error("Job got rejected {}", e.getMessage());
        }
    }

}
