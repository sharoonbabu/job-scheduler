package com.ad2pro.spectra.jobscheduler.service;

import com.ad2pro.spectra.jobscheduler.config.JobProperties;
import com.ad2pro.spectra.jobscheduler.domain.Job;
import com.ad2pro.spectra.jobscheduler.helpers.S3Helper;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.ad2pro.spectra.jobscheduler.utils.FileUtils.getFileName;

@Service
@Slf4j
public class JobProcessingService {

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private JobProperties jobProperties;

    @Autowired
    private S3Helper s3Helper;

    public Job buildJobMetadata(long siteId, String path, boolean possibleDup) {
        String filename = getFileName(path);
        Job job = getJobInfo(siteId, filename, possibleDup);
        job.setSiteId(siteId);
        return job;
    }

    public void processJobOrder(Job job) throws IOException {
        log.info("Consumed Job {}", job.getTrackingID());
        long siteID = job.getSiteId();
        Path jobPath = Paths.get(job.getPath());

//      TODO: Call policy service and decide validations to be done
       /* Path xmlFile  = getXMLPath(jobPath);
        JSONObject xmlObject = convertXMLToJSON(xmlFile);
        log.info(xmlObject.toString());
        validateJobXML(xmlObject, job);*/

        s3Helper.uploadJob(job);
//      TODO: Push event to kafka
    }

    private Job getJobInfo(long siteID, String filename, boolean isProduced) {
//        TODO: Modify to a detailed target path
        String targetFilename = jobProperties.getBaseTarget() + File.separator + siteID +
                File.separator + filename;
        Path targetPath = Paths.get(targetFilename);
        log.info("Intermediate path {}", targetPath);
        File file = new File(targetPath.toAbsolutePath().toString());
        return new Job("1", targetPath.getFileName().toString(), targetPath.toAbsolutePath().toString(),
                file.length(), isProduced);
    }

    private Path getXMLPath(Path jobPath) {
//      TODO: Extract [job].zip to get location of XML
        return jobPath;
    }

    private void validateJobXML(JSONObject xmlObject, Job job){
        KieSession kieSession = kieContainer.getKieBase().newKieSession();
        kieSession.setGlobal("job", job);
        kieSession.insert(xmlObject);
        kieSession.fireAllRules();
        kieSession.dispose();
    }
}
