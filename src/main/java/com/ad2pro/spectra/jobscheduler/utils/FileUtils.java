package com.ad2pro.spectra.jobscheduler.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
public class FileUtils {

    public static void listFiles(String directoryName, List<File> files) {
        log.info("Listing files in {}", directoryName);
        File directory = new File(directoryName);
        File[] fList = directory.listFiles();
        if(fList != null)
            for (File file : fList) {
                if (file.isFile()) {
                    files.add(file);
                } else if (file.isDirectory()) {
                    listFiles(file.getAbsolutePath(), files);
                }
            }
    }

    public static void deleteFile(String path){
        try {
            if(Files.exists(Paths.get(path))) {
                Files.delete(Paths.get(path));
                log.info("File deleted {}", path);
            }
        } catch (IOException e) {
            log.error("Cannot delete symbolic link {}, {}", path, e.getMessage());
        }
    }


    public static void move(String source, String target) throws IOException {
        log.info("Moving from {} to {}", source, target);
        Path sourcePath = Paths.get(source);
        Path targetPath = Paths.get(target);
        Files.move(sourcePath,targetPath);
        log.info("File moved {}", targetPath);
    }

    public static String getFileName(String path) {
        File f = new File(path);
        String filename = f.getAbsolutePath().substring(f.getAbsolutePath().lastIndexOf("\\") + 1);
        return filename;
    }



    public static String getFileContent(Path filePath) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = Files.newBufferedReader(filePath);
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line).append("\n");
        }
        return sb.toString();
    }
}

