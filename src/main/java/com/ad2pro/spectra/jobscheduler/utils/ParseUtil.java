package com.ad2pro.spectra.jobscheduler.utils;

import com.ad2pro.spectra.jobscheduler.exception.JobPathParseException;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.json.XML;

import java.io.IOException;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.ad2pro.spectra.jobscheduler.utils.FileUtils.getFileContent;

/**
 * @author Sharoon Babu
 * Package: com.ad2pro.spectra.jobscheduler.utils
 * Date: 20/6/19
 */
@Slf4j
public class ParseUtil {

    public static int PRETTY_PRINT_INDENT_FACTOR = 4;
    private static Pattern pattern = Pattern.compile("/(\\d+/\\w+)");

    public static long getSiteId(String path) throws JobPathParseException {
//        TODO: Modify logic to parse siteID from job path
        log.info("Parsing {} for siteID", path);
        Matcher matcher = pattern.matcher(path);
        if(matcher.find()) {
            long siteID = Long.parseLong(matcher.group());
            return siteID;
        }
        throw new JobPathParseException("Unable to parse siteID from path " + path);
    }

    public static JSONObject convertXMLToJSON(Path xmlPath) throws IOException {
        String xml = getFileContent(xmlPath);
        JSONObject xmlJSONObj = XML.toJSONObject(xml);
        return xmlJSONObj;
    }

}
