package com.ad2pro.spectra.jobscheduler.utils;

public final class Constants {

    public static final int QUEUE_MAX_SIZE = 15000;
    public static final int MAX_THREADS = 8;

}
