package com.ad2pro.spectra.jobscheduler.processors;

import com.ad2pro.spectra.jobscheduler.domain.Job;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

/**
 * @author Sharoon Babu
 * Package: com.ad2pro.spectra.jobscheduler.processors
 * Date: 20/6/19
 */

@Service
@Slf4j
public class QueueProcessor {


    public void pushIntoQueue(BlockingQueue<Job> jobQueue, Job job) throws IOException, InterruptedException {
        log.info("New job - {}", job);
        jobQueue.put(job);
    }
}
